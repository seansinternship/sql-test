USE [MovieReviews]
GO
/****** Object:  Table [dbo].[Actor]    Script Date: 3/18/2015 11:42:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actor](
	[ActorID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[DateOfBirth] [date] NULL,
 CONSTRAINT [PK_Actor] PRIMARY KEY CLUSTERED 
(
	[ActorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Movie]    Script Date: 3/18/2015 11:42:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movie](
	[MovieID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[ReleaseYear] [int] NOT NULL,
 CONSTRAINT [PK_Movie] PRIMARY KEY CLUSTERED 
(
	[MovieID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovieActor]    Script Date: 3/18/2015 11:42:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieActor](
	[MovieActorID] [int] IDENTITY(1,1) NOT NULL,
	[MovieID] [int] NOT NULL,
	[ActorID] [int] NOT NULL,
 CONSTRAINT [PK_MovieActor] PRIMARY KEY CLUSTERED 
(
	[MovieActorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovieReview]    Script Date: 3/18/2015 11:42:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MovieReview](
	[MovieReviewID] [int] IDENTITY(1,1) NOT NULL,
	[MovieID] [int] NOT NULL,
	[Review] [nvarchar](max) NOT NULL,
	[ScoreOutOfTen] [int] NOT NULL,
	[FlaggedInappropriate] [bit] NOT NULL CONSTRAINT [DF_MovieReview_FlaggedInappropriate]  DEFAULT ((0)),
	[IsVisible] [bit] NOT NULL CONSTRAINT [DF_MovieReview_IsVisible]  DEFAULT ((1)),
 CONSTRAINT [PK_MovieReview] PRIMARY KEY CLUSTERED 
(
	[MovieReviewID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Actor] ON 

INSERT [dbo].[Actor] ([ActorID], [FirstName], [LastName], [DateOfBirth]) VALUES (1, N'Lilly', N'James', NULL)
INSERT [dbo].[Actor] ([ActorID], [FirstName], [LastName], [DateOfBirth]) VALUES (2, N'Cate', N'Blanchett', NULL)
INSERT [dbo].[Actor] ([ActorID], [FirstName], [LastName], [DateOfBirth]) VALUES (3, N'Liam', N'Neeson', NULL)
INSERT [dbo].[Actor] ([ActorID], [FirstName], [LastName], [DateOfBirth]) VALUES (4, N'Ed', N'Harris', NULL)
SET IDENTITY_INSERT [dbo].[Actor] OFF
SET IDENTITY_INSERT [dbo].[Movie] ON 

INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (6, N'Chappie', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (1, N'Cinderella', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (11, N'Fifty Shades of Grey', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (4, N'Focus', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (3, N'Kingsman: The Secret Service', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (9, N'McFarland USA', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (2, N'Run All Night', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (12, N'The DUFF', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (7, N'The Second Best Exotic Marigold Hotel', 2015)
INSERT [dbo].[Movie] ([MovieID], [Title], [ReleaseYear]) VALUES (8, N'The SpongeBob Movie: Sponge Out of Water', 2015)
SET IDENTITY_INSERT [dbo].[Movie] OFF
SET IDENTITY_INSERT [dbo].[MovieActor] ON 

INSERT [dbo].[MovieActor] ([MovieActorID], [MovieID], [ActorID]) VALUES (1, 1, 1)
INSERT [dbo].[MovieActor] ([MovieActorID], [MovieID], [ActorID]) VALUES (2, 1, 2)
INSERT [dbo].[MovieActor] ([MovieActorID], [MovieID], [ActorID]) VALUES (3, 2, 3)
INSERT [dbo].[MovieActor] ([MovieActorID], [MovieID], [ActorID]) VALUES (4, 2, 4)
SET IDENTITY_INSERT [dbo].[MovieActor] OFF
SET IDENTITY_INSERT [dbo].[MovieReview] ON 

INSERT [dbo].[MovieReview] ([MovieReviewID], [MovieID], [Review], [ScoreOutOfTen], [FlaggedInappropriate], [IsVisible]) VALUES (1, 1, N'Elegant, enchanting re-imagining of the romantic folk tale for the 21st century.', 8, 0, 1)
INSERT [dbo].[MovieReview] ([MovieReviewID], [MovieID], [Review], [ScoreOutOfTen], [FlaggedInappropriate], [IsVisible]) VALUES (2, 1, N'Most fairy tales on film contain either enough wit or verve or spectacle to engage an entire audience, but there is nothing here to engage the adult viewer. Nothing. ', 2, 0, 1)
INSERT [dbo].[MovieReview] ([MovieReviewID], [MovieID], [Review], [ScoreOutOfTen], [FlaggedInappropriate], [IsVisible]) VALUES (3, 2, N'In the main, Jaume Collet-Serra is making a character-driven drama about betrayed honor, and the result is a film closer in spirit to the baggage-rich crime novels of Dennis Lehane than dumb multiplex fare. ', 6, 0, 1)
INSERT [dbo].[MovieReview] ([MovieReviewID], [MovieID], [Review], [ScoreOutOfTen], [FlaggedInappropriate], [IsVisible]) VALUES (4, 2, N'Gritty, fast-paced - and quickly forgettable.', 3, 0, 1)
SET IDENTITY_INSERT [dbo].[MovieReview] OFF
ALTER TABLE [dbo].[MovieActor]  WITH CHECK ADD  CONSTRAINT [FK_MovieActor_Actor] FOREIGN KEY([ActorID])
REFERENCES [dbo].[Actor] ([ActorID])
GO
ALTER TABLE [dbo].[MovieActor] CHECK CONSTRAINT [FK_MovieActor_Actor]
GO
ALTER TABLE [dbo].[MovieActor]  WITH CHECK ADD  CONSTRAINT [FK_MovieActor_Movie] FOREIGN KEY([MovieID])
REFERENCES [dbo].[Movie] ([MovieID])
GO
ALTER TABLE [dbo].[MovieActor] CHECK CONSTRAINT [FK_MovieActor_Movie]
GO
ALTER TABLE [dbo].[MovieReview]  WITH CHECK ADD  CONSTRAINT [FK_MovieReview_Movie] FOREIGN KEY([MovieID])
REFERENCES [dbo].[Movie] ([MovieID])
GO
ALTER TABLE [dbo].[MovieReview] CHECK CONSTRAINT [FK_MovieReview_Movie]
GO
